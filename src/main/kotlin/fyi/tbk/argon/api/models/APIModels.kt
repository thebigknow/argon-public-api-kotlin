package fyi.tbk.argon.api.models

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import moe.banana.jsonapi2.HasMany
import moe.banana.jsonapi2.HasOne
import moe.banana.jsonapi2.JsonApi
import moe.banana.jsonapi2.Resource
import java.time.Instant

@JsonClass(generateAdapter = true)
data class TokenRequest(
    @field:Json(name = "client_id") val client_id: String,
    @field:Json(name = "client_secret") val client_secret: String,
    @field:Json(name = "grant_type") val grant_type: String = "client_credentials",
    @field:Json(name = "scope") val scope: String = "read"
)

@JsonClass(generateAdapter = true)
data class TokenResponse(
    val access_token: String,
    val token_type: String,
    // Epoch seconds
    val expires_at: Int
) {
    fun expirationDate(): Instant = Instant.ofEpochSecond(this.expires_at.toLong())

    fun isExpired(now: Instant = Instant.now()) = now >= expirationDate()

    fun authToken() = "Bearer $access_token"
}

@JsonClass(generateAdapter = true)
data class JsonAPIErrors(
    val errors: List<JsonAPIError>
)

@JsonClass(generateAdapter = true)
data class JsonAPIError(
    val source: ErrorPointer,
    val detail: String
) {
    var code: Int? = null
}

@JsonClass(generateAdapter = true)
data class ErrorPointer(
    val pointer: String
)

@JsonApi(type = "users")
class User : Resource() {
    lateinit var identifier: String
    lateinit var email: String

    @field:Json(name = "first_name")
    lateinit var firstName: String

    @field:Json(name = "last_name")
    lateinit var lastName: String

    @field:Json(name = "jwt")
    lateinit var accessToken: String
}

@JsonClass(generateAdapter = true)
data class Trailer(
    val provider: String,
    val code: String,
    val transcript: String?
)

@JsonApi(type = "courses")
class Course : Resource() {
    @field:Json(name = "lessons")
    lateinit var lessons_has_many: HasMany<Lesson>

    lateinit var identifier: String
    lateinit var title: String

    @field:Json(name = "overview_intro")
    lateinit var overviewIntro: String

    @field:Json(name = "lesson_count")
    var lessonCount: Int = 0

    @field:Json(name = "duration")
    var durationInMinutes: Int? = null
    var trailer: Trailer? = null
    lateinit var tags: List<String>

    fun lessonIds(): List<String> = lessons_has_many.map { it.id }
    fun lessons(): List<Lesson> = lessons_has_many.get(document)
}

@JsonApi(type = "lessons")
class Lesson : Resource() {
    @field:Json(name = "course")
    lateinit var course_has_one: HasOne<Course>

    @field:Json(name = "instructor")
    lateinit var instructor_has_one: HasOne<Instructor>

    lateinit var identifier: String
    lateinit var title: String

    @field:Json(name = "card_description")
    lateinit var cardDescription: String
    var trailer: Trailer? = null
    lateinit var tags: List<String>

    fun courseId(): String = course_has_one.get().id
    fun course(): Course = course_has_one.get(document)
    fun instructorId(): String = instructor_has_one.get().id
    fun instructor(): Instructor = instructor_has_one.get(document)
}

@JsonApi(type = "instructors")
class Instructor : Resource() {
    lateinit var identifier: String
    var prefix: String? = null
    lateinit var title: String

    @field:Json(name = "first_name")
    lateinit var firstName: String

    @field:Json(name = "last_name")
    lateinit var lastName: String

    @field:Json(name = "full_name")
    lateinit var fullName: String
    lateinit var bio: String
    var trailer: Map<String, String?>? = null

    @field:Json(name = "detail_image")
    var detailImage: String? = null
}
