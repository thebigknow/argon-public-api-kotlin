package fyi.tbk.argon.api

import com.squareup.moshi.Moshi
import fyi.tbk.argon.api.models.*
import moe.banana.jsonapi2.Document
import moe.banana.jsonapi2.ResourceAdapterFactory
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * A thin wrapper around [APIInterface] that handles sync/and async in a more consistent way.
 *
 * @property apiInterface The Retrofit2 interface we are wrapping.
 */
class APIClient(val apiInterface: APIInterface) {
    private val jsonParser: Moshi by lazy {
        Moshi.Builder()
            .add(ResourceAdapterFactory.builder().build())
            .add(Document::class.java)
            .build()
    }

    fun getUser(identifier: String, partnerSubdomain: String): User? {
        val response = apiInterface.getUser(identifier, partnerSubdomain).execute()
        if (response.isSuccessful) return response.body()
        throw APIError("Could not retrieve User.")
    }

    fun putUser(identifier: String, partnerSubdomain: String, user: User): User? {
        return apiInterface.putUser(identifier, partnerSubdomain, user).execute().body()
    }

    /**
     * The synchronous version for fetching all Courses
     */
    fun listCourses(query: String? = null): APIResponse<Array<Course>?, JsonAPIError> {
        return APISyncHandler<Array<Course>, JsonAPIError>().onResponse(
            apiInterface.listCourses(query).execute()
        )
    }
    fun listCoursesWithRelations(): APIResponse<Array<Course>?, JsonAPIError> {
        return listCourses("lessons.instructor")
    }

    /**
     * The asynchronous version for fetching all Courses
     */
    fun listCourses(query: String? = null, callback: (APIResponse<Array<Course>?, JsonAPIError>) -> Unit) {
        return apiInterface.listCourses(query).enqueue(APIAsyncCallback<Array<Course>, JsonAPIError>(callback))
    }
    fun listCoursesWithRelations(callback: (APIResponse<Array<Course>?, JsonAPIError>) -> Unit) {
        listCourses("lessons.instructor", callback)
    }

    // Sync
    fun getCourse(courseIdentifier: String, query: String? = null): APIResponse<Course?, JsonAPIError> {
        return APISyncHandler<Course, JsonAPIError>().onResponse(
            apiInterface.getCourse(courseIdentifier, query).execute()
        )
    }
    fun getCourseWithRelations(courseIdentifier: String): APIResponse<Course?, JsonAPIError> {
        return getCourse(courseIdentifier, "lessons.instructor")
    }

    // Async
    fun getCourse(courseIdentifier: String, query: String? = null, callback: (APIResponse<Course?, JsonAPIError>) -> Unit) {
        apiInterface.getCourse(courseIdentifier, query).enqueue(APIAsyncCallback<Course, JsonAPIError>(callback))
    }
    fun getCourseWithRelations(courseIdentifier: String, callback: (APIResponse<Course?, JsonAPIError>) -> Unit) {
        apiInterface.getCourse(courseIdentifier, "lessons.instructor").enqueue(APIAsyncCallback<Course, JsonAPIError>(callback))
    }

    // Sync
    fun listCourseLessons(courseIdentifier: String, query: String? = null): APIResponse<Array<Lesson>?, JsonAPIError> {
        return APISyncHandler<Array<Lesson>, JsonAPIError>().onResponse(
            apiInterface.listCourseLessons(courseIdentifier, query).execute()
        )
    }
    fun listCourseLessonsWithRelations(courseIdentifier: String): APIResponse<Array<Lesson>?, JsonAPIError> {
        return listCourseLessons(courseIdentifier, "course,instructor")
    }

    // Async
    fun listCourseLessons(courseIdentifier: String, query: String? = null, callback: (APIResponse<Array<Lesson>?, JsonAPIError>) -> Unit) {
        apiInterface.listCourseLessons(courseIdentifier, query).enqueue(APIAsyncCallback<Array<Lesson>,JsonAPIError>(callback))
    }
    fun listCourseLessonsWithRelations(courseIdentifier: String, callback: (APIResponse<Array<Lesson>?, JsonAPIError>) -> Unit) {
        listCourseLessons(courseIdentifier, "course,instructor", callback)
    }

    // Sync
    fun getCourseLesson(courseIdentifier: String, lessonIdentifier: String, query: String? = null): APIResponse<Lesson?, JsonAPIError> {
        return APISyncHandler<Lesson, JsonAPIError>().onResponse(
            apiInterface.getCourseLesson(courseIdentifier, lessonIdentifier, query).execute()
        )
    }
    fun getCourseLessonWithRelations(courseIdentifier: String, lessonIdentifier: String): APIResponse<Lesson?, JsonAPIError> {
        return getCourseLesson(courseIdentifier, lessonIdentifier, "course,instructor")
    }

    // Async
    fun getCourseLesson(courseIdentifier: String, lessonIdentifier: String, query: String? = null, callback: (APIResponse<Lesson?, JsonAPIError>) -> Unit) {
        apiInterface.getCourseLesson(courseIdentifier, lessonIdentifier, query).enqueue(APIAsyncCallback<Lesson,JsonAPIError>(callback))
    }
    fun getCourseLessonWithRelations(courseIdentifier: String, lessonIdentifier: String, callback: (APIResponse<Lesson?, JsonAPIError>) -> Unit) {
        getCourseLesson(courseIdentifier, lessonIdentifier, "course,instructor", callback)
    }

    // Sync
    fun getLesson(lessonId: String, query: String? = null): APIResponse<Lesson?, JsonAPIError> {
        return APISyncHandler<Lesson, JsonAPIError>().onResponse(
            apiInterface.getLesson(lessonId, query).execute()
        )
    }
    fun getLessonWithRelations(lessonId: String): APIResponse<Lesson?, JsonAPIError> {
        return getLesson(lessonId, "course,instructor")
    }

    // Async
    fun getLesson(lessonId: String, query: String? = null, callback: (APIResponse<Lesson?, JsonAPIError>) -> Unit) {
        apiInterface.getLesson(lessonId, query).enqueue(APIAsyncCallback<Lesson,JsonAPIError>(callback))
    }
    fun getLessonWithRelations(lessonId: String, callback: (APIResponse<Lesson?, JsonAPIError>) -> Unit) {
        return getLesson(lessonId, "course,instructor", callback)
    }

    // Sync
    fun listInstructors(): APIResponse<Array<Instructor>?, JsonAPIError> {
        return APISyncHandler<Array<Instructor>, JsonAPIError>().onResponse(
            apiInterface.listInstructors().execute()
        )
    }

    // Async
    fun listInstructors(callback: (APIResponse<Array<Instructor>?, JsonAPIError>) -> Unit) {
        apiInterface.listInstructors().enqueue(APIAsyncCallback<Array<Instructor>,JsonAPIError>(callback))
    }

    // Sync
    fun getInstructor(instructorIdentifier: String, query: String? = null): APIResponse<Instructor?, JsonAPIError> {
        return APISyncHandler<Instructor, JsonAPIError>().onResponse(
            apiInterface.getInstructor(instructorIdentifier, query).execute()
        )
    }

    // Async
    fun getInstructor(instructorIdentifier: String, query: String? = null, callback: (APIResponse<Instructor?, JsonAPIError>) -> Unit) {
        apiInterface.getInstructor(instructorIdentifier, query).enqueue(APIAsyncCallback<Instructor,JsonAPIError>(callback))
    }
}

// "callback" is the callback that is passed into the API function. After the async call resolves it will
// invoke the callback with an ApiResponse<Success,Failure> object.
class APIAsyncCallback<L, R>(private val callback: (APIResponse<L?, R>) -> Unit) : Callback<L>, APIErrorHandler {
    override fun onResponse(call: Call<L>, response: Response<L>) {
        if (response.isSuccessful) callback.invoke(APIResponse.of(response.body()))
        else {
            callback.invoke(APIResponse.error(parseError(response.code(), response.errorBody()?.string()) as R))
        }
    }

    override fun onFailure(call: Call<L>, t: Throwable) {
        TODO("Not yet implemented")
    }
}

class APISyncHandler<L, R> : APIErrorHandler {
    fun onResponse(response: Response<L>): APIResponse<L?, R> {
        return if (response.isSuccessful) {
            APIResponse.of(response.body())
        } else {
            val error = parseError(response.code(), response.errorBody()?.string())
            APIResponse.error(error as R)
        }
    }
}

interface APIErrorHandler {
    fun parseError(code: Int, error: String?): JsonAPIError {
        val errors: JsonAPIErrors? = Moshi.Builder()
            .build()
            .adapter(JsonAPIErrors::class.java)
            .fromJson(error)
        return (errors?.errors?.get(0) ?: JsonAPIError(
            ErrorPointer(this.javaClass.name),
            "Unknown Error"
        )).also {
            it.code = code
        }
    }
}
