package fyi.tbk.argon.api

open class StandardError(message: String?) : Throwable(message)
class AuthenticationError(message: String?) : StandardError(message)
class APIError(message: String?) : StandardError(message)