package fyi.tbk.argon.api

import com.squareup.moshi.Moshi
import fyi.tbk.argon.api.models.*
import moe.banana.jsonapi2.Document
import moe.banana.jsonapi2.JsonApiConverterFactory
import moe.banana.jsonapi2.ResourceAdapterFactory
import okhttp3.*
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

object APIClientFactory {
    fun buildRawAPI(base_url: String, client_id: String, client_secret: String): APIInterface {
        val client = OkHttpClient.Builder()
            .addInterceptor(OAuthInterceptor(base_url, client_id, client_secret))
            .build()

        val jsonApiFactory: ResourceAdapterFactory = ResourceAdapterFactory.builder()
            .add(Instructor::class.java)
            .add(Course::class.java)
            .add(Lesson::class.java)
            .add(User::class.java)
            .build()

        val moshi: Moshi = Moshi.Builder()
            .add(jsonApiFactory)
            .build()

        return Retrofit.Builder()
            .baseUrl(base_url)
            .addConverterFactory(JsonApiConverterFactory.create(moshi))
            .addConverterFactory(MoshiConverterFactory.create())
            .client(client)
            .build()
            .create(APIInterface::class.java)
    }

    fun build(base_url: String, client_id: String, client_secret: String): APIClient {
        return APIClient(buildRawAPI(base_url, client_id, client_secret))
    }
}

class OAuthInterceptor(val base_url: String, val client_id: String, val client_secret: String) : Interceptor {
    private var tokenResponse: TokenResponse? = null

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        if (tokenResponse?.isExpired() != false) authenticate()
        val newRequest = request.newBuilder()
            .header("Authorization", tokenResponse!!.authToken())
            .build()
        return chain.proceed(newRequest)
    }

    private fun authenticate() {
        val moshi = Moshi.Builder().build()
        val reqadapter = moshi.adapter(TokenRequest::class.java)
        val json = reqadapter.toJson(TokenRequest(client_id, client_secret))

        val client = OkHttpClient.Builder().build()
        val request = Request.Builder()
            .url("${base_url}/token")
            .post(RequestBody.create(MediaType.get("application/json"), json))
            .build()
        val response = client.newCall(request).execute()
        if (!response.isSuccessful || response.body() == null) throw AuthenticationError("Could not authenticate")
        val resadapter = moshi.adapter(TokenResponse::class.java)
        this.tokenResponse = resadapter.fromJson(response.body()!!.string())
    }
}
