package fyi.tbk.argon.api

/**
 * A flexible response pattern
 *
 * This pattern was copied from https://www.scottbrady91.com/Kotlin/Experimenting-with-Kotlin-and-OAuth.
 * It's very flexible because it allows chaining with the `bind` method and if there is a failure anywhere along
 * the chain it doesn't matter because it will just pass the [Failure] response all the way up.
 *
 * Usage: The below will detect an error in the second bind and the third bind will not run.
 * <pre>
 * {@code
 * val resp = APIResponse.of<String, String>("foo")
 *                       .bind { str -> APIResponse.of("$str bar") }
 *                       .bind { _ -> APIResponse.error<String, String>("Fake error here")}
 *                       .bind { str -> APIResponse.of("$str Baz")}
 * when (r) {
 *     is APIResponse.Success -> println(r.value)
 *     is APIResponse.Failure -> println(r.error)
 * }
 * }
 * </pre>
 */
sealed class APIResponse<out L, R> {
    class Success<out L, R>(val value: L) : APIResponse<L, R>()
    class Failure<out L, R>(val error: R) : APIResponse<L, R>()

    /**
     * Failure resistant chaining support
     *
     * @param success a function to call if we are in a Success state
     */
    fun <X> bind(success: (L) -> (APIResponse<X, R>)): APIResponse<X, R> {
        return when (this) {
            is Success<L, R> -> success(this.value)
            is Failure<L, R> -> Failure(this.error)
        }
    }

    companion object {
        fun <L, R> of(response: L) = Success<L, R>(response)
        fun <L, R> error(error: R) = Failure<L, R>(error)
    }
}
