package fyi.tbk.argon.api

import fyi.tbk.argon.api.models.Course
import fyi.tbk.argon.api.models.Instructor
import fyi.tbk.argon.api.models.Lesson
import fyi.tbk.argon.api.models.User
import retrofit2.Call
import retrofit2.http.*

interface APIInterface {
    @GET(value = "users/{identifier}")
    fun getUser(
        @Path("identifier") identifier: String,
        @Query("partner") partnerSubdomain: String
    ): Call<User>

    @PUT(value = "users/{identifier}")
    fun putUser(
        @Path("identifier") identifier: String,
        @Query("partner") partnerSubdomain: String,
        @Body user: User
    ): Call<User>

    @GET(value = "courses")
    fun listCourses(
        @Query("include") query: String?
    ): Call<Array<Course>>

    @GET(value = "courses/{course}")
    fun getCourse(
        @Path("course") courseIdentifier: String,
        @Query("include") query: String?
    ): Call<Course>

    @GET(value = "courses/{course}/lessons")
    fun listCourseLessons(
        @Path("course") courseIdentifier: String,
        @Query("include") query: String?
    ): Call<Array<Lesson>>

    @GET(value = "courses/{course}/lessons/{lesson}")
    fun getCourseLesson(
        @Path("course") courseIdentifier: String,
        @Path("lesson") lessonIdentifier: String,
        @Query("include") query: String?
    ): Call<Lesson>

    @GET(value = "lessons/{lesson}")
    fun getLesson(
        @Path("lesson") lessonId: String,
        @Query("include") query: String?
    ): Call<Lesson>

    @GET(value = "instructors")
    fun listInstructors(): Call<Array<Instructor>>

    @GET(value = "instructors/{instructor}")
    fun getInstructor(
        @Path("instructor") instructorIdentifier: String,
        @Query("include") query: String?
    ): Call<Instructor>
}
