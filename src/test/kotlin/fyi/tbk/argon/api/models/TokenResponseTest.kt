package fyi.tbk.argon.api.models

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.time.Instant

class TokenResponseTest {
    val resp = TokenResponse("fake_access_token", "Bearer", 1607212800)

    @Test
    fun `expirationDate is in ISO8601 format`() {
        Assertions.assertEquals("2020-12-06T00:00:00Z", resp.expirationDate().toString())
    }

    @Test
    fun `it is expired when the current time is the same as expirationDate`() {
        Assertions.assertTrue(resp.isExpired(resp.expirationDate()))
    }

    @Test
    fun `it is expired when the current time is greater than the expirationDate`() {
        Assertions.assertTrue(resp.isExpired(Instant.now()))
    }

    @Test
    fun `it is NOT expired when the current time is less than the expirationDate`() {
        val now = Instant.ofEpochSecond(1607212799)
        Assertions.assertFalse(resp.isExpired(now))
    }

    @Test
    fun `it returns a formatted Authentication token`() {
        Assertions.assertEquals("Bearer fake_access_token", resp.authToken())
    }
}