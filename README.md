TBK Public API Client
=====================

This is a client for The Big Know's public API for the Argon platform. It is written in Kotlin so it has a better experience
in that language but it can be just as easily used in a Java project. I will give examples for each below.

In general, each endpoint provides at least two overridden methods; one that provides synchronous access that returns an APIResponse and another that is asynchronous and returns nothing but takes a callback that is fed an APIResponse object.

## Installation

Currently this is not published to Maven Central so you'll need to download the jar file. There are two versions of this; one that is just this library and one that has all of the needed dependencies already in the Jar.

If you would like you manage your own dependencies you can use the following Jar. It has a `pom.xml` at the path `META-INF/maven/fyi.tbk.argon/api/pom.xml`.

```
curl -OJ https://bitbucket.org/thebigknow/argon-public-api-kotlin/downloads/fyi.tbk.argon.api-1.0.0.jar
```

If you want the "fat" jar with all of the dependencies you can get that here:

```
curl -OJ https://bitbucket.org/thebigknow/argon-public-api-kotlin/downloads/fyi.tbk.argon.api-1.0.0-all.jar
```

## Authentication

Authentication uses a standard OAuth2 client_crendentials grant approach. All of that logic is taken care of in the code so all you have to do is feed `APIClientFactory` a `baseUrl`, `clientId` and `clientSecret`. Here is how you can do that:

**Kotlin**
```kotlin
val baseUrl = "httpsl://api.env.domain.here/api/public/v1/";
val clientId = "<CLIENT_ID>";
val clientSecret = "<CLIENT_SECRET>";
val client = APIClientFactory.build(baseUrl, clientId, clientSecret);
```

**Java**
```java
String baseUrl = "httpsl://api.env.domain.here/api/public/v1/";
String clientId = "<CLIENT_ID>";
String clientSecret = "<CLIENT_SECRET>";
APIClient client = APIClientFactory.INSTANCE.build(baseUrl, clientId, clientSecret);
```


## Using the API

Once you have an `APIClient` instance you can now talk to the API. All of the available methods can be found in `APIClient.kt`. For demonstration purposes I will show you how to list courses both synchronously and asynchronously. The basic method for this is called `listCourses()` however, if you need to use `Lessons` or `Instructors` I would suggest using `listCoursesWithRelations()` which will load all of the associated `Lessons` and `Instructors` at the same time so you don't have to make multiple calls to the API.

**Kotlin**
```kotlin
// Sync
val courses = client.listCourses()
when (course) {
  is APIResponse.Success -> {
    it.value!!.iterator().forEach { cit -> println("Course: ${cit.identifier}") }
  }
  is APIResponse.Failure -> {
      println("Error: ${it.error.code}, ${it.error.detail}")
  }
}

// Async
client.listCourses() {
    when (it) {
        is APIResponse.Success -> {
            it.value!!.iterator().forEach { cit -> println("Course: ${cit.identifier}") }
        }
        is APIResponse.Failure -> {
            println("Error: ${it.error.code}, ${it.error.detail}")
        }
    }
}
```

**Java**

```java
// Sync
APIResponse<Course[], JsonAPIError> courses = client.listCourses(null);
if (courses instanceof APIResponse.Success) {
    for (Course course : ((APIResponse.Success<Course[], JsonAPIError>) courses).getValue()) {
        System.out.println("Course: " + course.getIdentifier());
    }
} else if (courses instanceof APIResponse.Failure) {
    JsonAPIError error = ((APIResponse.Failure<Course[], JsonAPIError>) courses).getError();
    System.out.println(error.getDetail());
}


// Async
client.listCourses(null, (response) -> {
  if (response instanceof APIResponse.Success) {
      for (Course course : ((APIResponse.Success<Course[], JsonAPIError>) courses).getValue()) {
          System.out.println("Async Course: " + course.getIdentifier());
      }
  } else if (courses instanceof APIResponse.Failure) {
      JsonAPIError error = ((APIResponse.Failure<Course[], JsonAPIError>) courses).getError();
      System.out.println(error.getDetail());
  }
  return null;
});

```
